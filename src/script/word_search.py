from pathlib import Path

def find_word(grid, word):
    rows = len(grid)
    cols = len(grid[0])
    directions = [(0, 1), (1, 0), (1, 1), (-1, 1), (0, -1), (-1, 0), (-1, -1), (1, -1)]
    for x in range(rows):
        for y in range(cols):
            for dx, dy in directions:
                nx, ny = x, y
                found = True
                for i in range(len(word)):
                    if 0 <= nx < rows and 0 <= ny < cols and grid[nx][ny] == word[i]:
                        nx += dx
                        ny += dy
                    else:
                        found = False
                        break
                if found:
                    end_x = nx - dx
                    end_y = ny - dy
                    return f"{word} {x}:{y} {end_x}:{end_y}"
    return None

def solve_word_search(input_file_path):
    # Read the input file
    with open(input_file_path, 'r') as file:
        lines = file.read().splitlines()
    
    # Parse the grid size
    grid_size = lines[0].split('x')
    rows = int(grid_size[0])
    
    # Extract the grid
    grid = [line.replace(' ', '') for line in lines[1:rows+1]]
    
    # Extract the words to find
    words = lines[rows+1:]
    
    # Search for each word and print the results
    for word in words:
        result = find_word(grid, word)
        if result:
            print(result)
    return
# For testing purposes
def solve_word_search_direct(rows, cols, grid_lines, words):
    # Adjusted function to take direct inputs
    grid = [line.replace(' ', '') for line in grid_lines]
    results = []
    for word in words:
        result = find_word(grid, word)
        if result:
            if result.split()[1] not in results:
                results.append(result)
    return results
p = Path(__file__).with_name('word_search_input.txt')

solve_word_search(p)




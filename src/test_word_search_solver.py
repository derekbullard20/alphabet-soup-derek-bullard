import unittest
from script.word_search import solve_word_search_direct

class TestWordSearch(unittest.TestCase):
    def test_basic_functionality(self):
        rows, cols = 3, 3
        grid = ["C A T", "D O G", "R A T"]
        words = ["CAT", "DOG", "RAT"]
        expected = ['CAT 0:0 0:2', 'DOG 1:0 1:2', 'RAT 2:0 2:2']
        self.assertEqual(solve_word_search_direct(rows, cols, grid, words), expected)
    
    def test_single_horizontal_word(self):
        rows, cols = 1, 5
        grid = ["W O R D S"]
        words = ["WORDS"]
        expected = ['WORDS 0:0 0:4']
        results = solve_word_search_direct(rows, cols, grid, words)
        self.assertEqual(results, expected)
    
    def test_single_vertical_word(self):
        rows, cols = 5, 1
        grid = ["W", "O", "R", "D", "S"]
        words = ["WORDS"]
        expected = ['WORDS 0:0 4:0']
        results = solve_word_search_direct(rows, cols, grid, words)
        self.assertEqual(results, expected)
    
    def test_word_not_present(self):
        rows, cols = 2, 2
        grid = ["A B", "C D"]
        words = ["NOT"]
        expected = []
        results = solve_word_search_direct(rows, cols, grid, words)
        self.assertEqual(results, expected)
if __name__ == '__main__':
    unittest.main()
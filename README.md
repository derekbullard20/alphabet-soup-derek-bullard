# Alphabet Soup

## Solution Explanation
If you have k words to find in the grid, and assuming each word search is independent of others, the total time complexity would be O(k×mn×l), where k is the number of words to search for. mn being grid size and l being word length


## Unit tests
The unit tests are structured like this for the following:
👨🏽‍💻Separation of Concerns: Keeps the application logic separate from the testing code.
📱Organization: Makes it easier for other developers to understand the structure of the project.
📡Flexibility: Allows users to run tests independently from the main application, which is useful for continuous integration environments.

## Running command for unit tests
```
cd src

python3 -m unittest test_word_search_solver.py
```

## Running index.py
```
python3 index.py
```